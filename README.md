# mdBook Theme

## Description

This project is a theme for [mdBook](https://github.com/rust-lang/mdBook). The theme is based on the layout of [GitBook](https://www.gitbook.com/).
<br />
<br />

## Screenshots

![img.png](docs/images/main-page-light.png)

## Installation

<br />
<br />

## Changes

New (added) css variables to theme:
 * `divider`
 * `header-bg`
 * `header-fg`
 * `page-toc-fg`
 * `page-toc-selected`
 * `sidebar-footer-bg`
 * `sidebar-footer-fg`
 * `sidebar-footer-hg`
 * `searchresults-hover-bg`
 * `scrollbar-small`
 * `scrollbar-small-hover`

Removed css variables:
 * `searchbar-border-color`
 * `searchbar-shadow-color`

<br />
<br />

## Authors

This theme is created by Luigi600.


<br />
<br />

## Licence


<br />
<br />

## TODOs

 * [x] Sidebar Footer Link Color
 * [ ] Prev/Next Chapter Buttons
 * [x] Right Sidebar Functionality
 * [x] Right Sidebar Layout
 * [x] Search Result Layout
 * [ ] Responsive Design
 * [ ] Adjust The Other Themes
 * [ ] CSS Variable Search Box Color
 * [ ] CSS Variable Select Colors
 * [ ] Fix Search Opening While Chapter Click
 * [ ] Printing  
 * [ ] GitLab CI/CD
